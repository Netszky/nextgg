import Image from 'next/image';
import React, { useState } from 'react';
import styled from 'styled-components';
import { AnimatePresence, motion } from 'framer-motion';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Champion = ({ champion }) => {

    const [isHovered, setIsHovered] = useState(false);
    const router = useRouter();

    return (
        <Card onClick={() => router.push(`/champions/${champion.id}`)} onMouseLeave={() => setIsHovered(false)} onMouseEnter={() => setIsHovered(true)} whileInView={{ x: 0, opacity: 1 }} initial={{ x: -50, opacity: 0 }} transition={{ duration: 0.8, ease: "easeInOut" }}>
            <ImageContainer>
                <Image priority sizes='100, 100' src={`http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${champion.id}_0.jpg`} alt={champion.name} style={{ objectFit: "cover", objectPosition: "80%" }} fill></Image>
            </ImageContainer>
            <CardContour />
            <AnimatePresence mode='wait'>
                {isHovered &&
                    <Name initial={{ y: -20, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: -20, opacity: 0 }} transition={{ duration: 0.6, ease: "easeInOut" }}>{champion.name}</Name>
                }
            </AnimatePresence>
        </Card>
    );
};

export default Champion;

export const ImageContainer = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
    will-change: transform;
    transform-origin:center;
    transform: perspective(800px) rotateY(20deg);
    transition:0.5s; 
    border-radius: 4px;
    opacity: 1;
    overflow: hidden;
    z-index: 1;
    

`

const Card = styled(motion.div)`
    height: 55%;
    width: 20%;
    margin: 40px 25px;
    border-radius: 6px;
    @media (min-width: 2000px){
        height: 45%;
        width: 15%;
    }
    cursor: pointer;
    position: relative;
    &:hover {
        ${ImageContainer} {
            opacity:1 !important;
            transform:perspective(800px) rotateY(0deg);
        }
    }
`
const Name = styled(motion.p)`
    color: #fff;
    position: absolute;
    right: -60px;
    bottom: 0;
    font-size: 22px;
    writing-mode: vertical-rl;
    text-transform: uppercase;

`
const CardContour = styled.div`
    position: absolute;
    background: transparent;
    border: 2px  solid #b6ab5b;
    height: 100%;
    width: 100%;
    top: 0;
    right: 0%;
    border-radius: 6px;
    z-index: 0;
`
