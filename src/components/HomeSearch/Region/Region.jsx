import React from 'react';
import styled from 'styled-components';

const Region = ({ selected, name, select }) => {
    return (
        <RegionCard active={selected === name ? true : false} onClick={() => select(name)}>
            <Text>{name}</Text>
        </RegionCard>
    );
};

export default Region;

const RegionCard = styled.span`
    width: 50px;
    height: 25px;
    border-radius: 4px;
    text-align: center;
    margin: 0;
    display: flex;  
    justify-content: center;
    align-items: center;
    background-color: ${props => props.active ? "red" : "#151a2f"};
    margin: 2px 5px;
    &:hover {
        background-color: ${props => props.active ? "#red" : "#293359"}
    }
    p {
        margin: 0;
    }
`

const Text = styled.p`

`