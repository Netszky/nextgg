import React, { useState } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Region from './Region/Region';
import Link from 'next/link';

const HomeSearch = () => {
    const [selectedRegion, setSelectedRegion] = useState("EUW");
    const [pseudo, setPseudo] = useState('');

    const region = ["EUW", "EUN", "NA", "KR", "BR", "JP", "RU", "OCE", "TR", "LAN", "LAS"]

    const selectRegion = (region) => {
        setSelectedRegion(region)
    }
    return (
        <Container>
            <SearchInput onChange={(e) => setPseudo(e.target.value)} initial={{ scaleX: 0 }} whileInView={{ scaleX: 1 }} transition={{ duration: 0.8, ease: "easeOut" }} placeholder={"Summoner's Name"} required type={"search"} />
            <RegionSelector>
                {region.map((reg, index) => {
                    return (
                        <Region name={reg} key={index} selected={selectedRegion} select={selectRegion} />
                    )
                })}
            </RegionSelector>
            <ButtonContainer>
                <Link href={{
                    pathname: "/app", query: {
                        "summoner": pseudo,
                        "region": selectedRegion
                    }
                }}>
                    <Button whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.9 }} transition={{ duration: 0.1 }}>Rechercher</Button>
                </Link>
            </ButtonContainer>
        </Container>
    );
};

export default HomeSearch;


const Container = styled(motion.form)`
    position: absolute;
    top: 30%;
    left:50%;
    transform: translateX(-50%);
    width: 800px;
    z-index: 3;
`
const SearchInput = styled(motion.input)`
    width: 100%;
    height: 100%;
    outline: none;
    height: 50px;
    background-color: #fff;
    border-radius: 30px;
    border: transparent;
    color: black;
    padding: 10px 60px 10px 24px;
    font-size: 22px;
`

const RegionSelector = styled(motion.div)`
    height: 40px;
    width: 100%;
    display: flex;
    justify-content: center;
    cursor: pointer;
    padding: 5px;
`
const ButtonContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    height: 200px;
    align-items: center;
`

const Button = styled(motion.button)`
    padding: 20px 50px;
    background-color: #576dc5;
    color: #151a2f;
    font-weight: bold;
    cursor: pointer;
    border-radius: 30px;
    border: none;
    outline: none;
`
