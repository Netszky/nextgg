import React, { useState } from 'react';
import styled from 'styled-components';
import { motion, AnimatePresence } from 'framer-motion'
import Region from '../HomeSearch/Region/Region';
import { useRouter } from 'next/router';

const Header = () => {
    const [searchSummoner, setSearchSummoner] = useState();
    const router = useRouter();
    const region = ["EUW", "EUN", "NA", "KR", "BR", "JP", "RU", "OCE", "TR", "LAN", "LAS"]
    const [toggleRegion, setToggleRegion] = useState(false);
    const [selectedRegion, setSelectedRegion] = useState("EUW")
    const selectRegion = (region) => {
        setSelectedRegion(region)
    }
    const submitHandler = (e) => {
        e.preventDefault()
        router.push({
            pathname: "/app", query: {
                "summoner": searchSummoner,
                "region": selectedRegion
            }
        })
    };

    return (
        <Container>
            <SearchContainer onSubmit={(e) => submitHandler(e)}>
                <Search onChange={(e) => setSearchSummoner(e.target.value)} required placeholder="Summoner's Name">
                </Search>
                <RegionCard onClick={() => setToggleRegion(!toggleRegion)}>{selectedRegion}</RegionCard>
                {/* <AnimatePresence>
                    {!toggleRegion &&
                        <RegionHider initial={{ scaleX: 0, transformOrigin: "right" }} animate={{ scaleX: 1, transformOrigin: "right" }} exit={{ scaleX: 0, transformOrigin: "right" }} transition={{ duration: 0.6, ease: "easeInOut" }} >

                        </RegionHider>
                    }

                </AnimatePresence> */}
                <RegionSelector >
                    {region.map((reg, index) => {
                        return (
                            <Region name={reg} key={index} selected={selectedRegion} select={selectRegion} />
                        )
                    })}
                </RegionSelector>
            </SearchContainer>
        </Container>
    );
};

export default Header;

const Container = styled.header`
    width: 100vw;
    height: 70px;
    padding-left: 60px;
    background-color: #0d101e;
    position: fixed;
    border-bottom: 1px solid #293359;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 100;
`
const SearchContainer = styled.form`
    width: 40%;
    height: 100%;
    display: flex;
    align-items: center;
    position: relative;
`

const Search = styled.input`
    width: 100%;
    height: 70%;
    border-radius: 4px;
    outline: none;
    border: none;
    padding: 5px 15px;
    background-color: #14182c;
    
`

const RegionHider = styled(motion.div)`
    height: 40px;
    width: 100%;
    display: flex;
    justify-content: center;
    padding: 5px;
    position: absolute;
    bottom: -60%;
    align-items:center;
    z-index: 56;
    background-color: #0d101e;
`
const RegionSelector = styled(motion.div)`
    height: 40px;
    width: 100%;
    display: flex;
    justify-content: center;
    cursor: pointer;
    padding: 5px;
    bottom: -60%;
    position: absolute;
    z-index: -1;
`
const RegionCard = styled.span`
    width: 50px;
    height: 25px;
    position: absolute;
    right: 5px;
    border-radius: 4px;
    text-align: center;
    margin: 0;
    display: flex;  
    justify-content: center;
    cursor: pointer;
    align-items: center;
    background-color: #293359;
    margin: 2px 5px;
    &:hover {
        background-color: ${props => props.active ? "#red" : "#293359"}
    }
    p {
        margin: 0;
    }
`