import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';

const Skill = ({ select, spell, type, index, selected }) => {
    return (
        <Container onClick={() => select(spell, index)}>
            <SkillImage active={selected?.id === spell?.id ? true : false}>
                {type === 'spell' ?
                    <Img alt='Spell image' sizes='100%,100%' fill src={`https://ddragon.leagueoflegends.com/cdn/12.22.1/img/spell/${spell?.image?.full}`}></Img>
                    :
                    <Img alt='Spell image' sizes='100%,100%' fill src={`https://ddragon.leagueoflegends.com/cdn/12.22.1/img/passive/${spell?.image?.full}`}></Img>
                }
            </SkillImage>
            <p>{spell?.name}</p>
        </Container>
    );
};

export default Skill;
const Container = styled.div`
    width: 20%;
    height:100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`
const SkillImage = styled.div`
    position: relative;
    width: 45px;
    cursor: pointer;
    height: 45px;
    overflow: hidden;
    border-radius: 2px;
    border: ${props => props.active ? "2px solid #b6ab5b" : "1px solid #000"};
    
`
const Img = styled(Image)`
    object-fit: cover;
`