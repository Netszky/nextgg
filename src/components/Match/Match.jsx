import axios from 'axios';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Image from 'next/image'
const Match = ({ match }) => {
    const [matchInfo, setMatchInfo] = useState();
    useEffect(() => {
        axios.get(`https://europe.api.riotgames.com/lol/match/v5/matches/${match}?api_key=${process.env.NEXT_PUBLIC_RIOT_API}`)
            .then((data) => {
                setMatchInfo(data.data)
                console.log("match info", data.data);
                console.log(matchInfo.participants);
            })
            .catch((err) => {

            })
    }, [match])

    return (
        <MatchContainer initial={{ x: -200 }} whileInView={{ x: 0 }} >
            <MatchDate>
                <p>{new Date(matchInfo?.info?.gameCreation).toLocaleDateString()}</p>
                <p>{matchInfo?.info?.gameMode}</p>
            </MatchDate>
            <TeamContainer>
                {[0, 1, 2, 3, 4].map((player) => {
                    return (
                        <PlayerContainer key={player}>
                            <ChampionImage>
                                <Img fill src={`http://ddragon.leagueoflegends.com/cdn/12.22.1/img/champion/${matchInfo?.info?.participants[player].championName}.png`}></Img>
                            </ChampionImage>
                            <PlayerName>{matchInfo?.info?.participants[player].summonerName}</PlayerName>
                        </PlayerContainer>
                    )
                })}

            </TeamContainer>
            <TeamContainer>
                {[5, 6, 7, 8, 9].map((player) => {
                    return (
                        <PlayerContainer key={player}>
                            <ChampionImage>
                                <Img fill src={`http://ddragon.leagueoflegends.com/cdn/12.22.1/img/champion/${matchInfo?.info?.participants[player].championName}.png`}></Img>
                            </ChampionImage>
                            <PlayerName>{matchInfo?.info?.participants[player].summonerName}</PlayerName>
                        </PlayerContainer>
                    )
                })}
            </TeamContainer>

        </MatchContainer>
    );
};

export default Match;


const MatchContainer = styled(motion.div)`
    width: 100%;
    height: 150px;
    margin: 10px 0px;
    border-radius: 6px;
    cursor: pointer;
    border: 1px solid #293359;
    display: flex;
    padding: 5px 10px;
`
const MatchDate = styled.div`
    height: 100%;
    display: flex;
    flex-direction:column;
    width: 10%;
    position: relative;
    align-items: center;
    justify-content: center;
    &::after {
        position: absolute;
        content: '';
        right: 0;
        height: 50%;
        width: 1px;
        background-color: #293359;
    }
`

const TeamContainer = styled.div`
    width: 25%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

const PlayerContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 50%;
`

const ChampionImage = styled.div`
    position: relative;
    width: 20px;
    height: 20px;
    margin-right: 8px;
`
const Img = styled(Image)`

`

const PlayerName = styled.p`
    margin: 0;
    width: 100%;
`