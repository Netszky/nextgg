import axios from 'axios';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Header from '../../components/Header/Header';
import Match from '../../components/Match/Match';
import Side from '../../components/Side/Side';
import getRegion from '../../utils/getRegion';
import Head from "next/head";

const Index = () => {
    const [profil, setProfil] = useState();
    const [error, setError] = useState(false);
    const router = useRouter();
    const [region, setRegion] = useState();
    const [name, setName] = useState();
    const [accountDetail, setAccountDetail] = useState();
    const [matchList, setMatchList] = useState([])

    useEffect(() => {
        setRegion(getRegion(router.query.region))
        setName(router.query.summoner)
    }, [router.query.summoner, router.query.region])



    useEffect(() => {
        if (region) {
            axios.get(`https://${region}.api.riotgames.com/lol/summoner/v4/summoners/by-name/${name}?api_key=${process.env.NEXT_PUBLIC_RIOT_API}`, {
            }).then((data) => {
                setProfil(data.data)
                console.log(data);
            }).catch((err) => {
                console.log(err.response);
                setError(true)
            })
        }
    }, [region, name])

    useEffect(() => {
        if (profil) {

            axios.get(`https://${region}.api.riotgames.com/lol/league/v4/entries/by-summoner/${profil?.id}?api_key=${process.env.NEXT_PUBLIC_RIOT_API}`, {
            }).then((data) => {
                setAccountDetail(data.data[0])
            }).catch((err) => {
                setError(true)
            })

            axios.get(`https://europe.api.riotgames.com/lol/match/v5/matches/by-puuid/${profil?.puuid}/ids?start=0&count=8&api_key=${process.env.NEXT_PUBLIC_RIOT_API}`, {
            }).then((data) => {
                setMatchList(data.data)
            }).catch((err) => {
                setError(true)
            })

        }
    }, [profil])

    return (
        <>
            <Head>
                <title>NEXT.GG | Ultimate LOL APP</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="http://ddragon.leagueoflegends.com/cdn/12.22.1/img/champion/Lux.png" />
            </Head>
            <Header />
            <Side />
            <Main>
                <ProfilContainer>
                    <ImageContainer>
                        <Image alt='Profile Icon' fill src={`https://ddragon.leagueoflegends.com/cdn/11.14.1/img/profileicon/${profil?.profileIconId}.png`}></Image>
                    </ImageContainer>
                    <InformationsContainer>
                        <Profilname>{profil?.name}</Profilname>
                        <Level>{profil?.summonerLevel}</Level>
                    </InformationsContainer>
                    <Image alt='Ranked Icon' src={"/Emblem_Challenger.png"} height={50} width={50}></Image>
                </ProfilContainer>
                <MatchContainer>
                    {matchList.map((item) => {
                        return (
                            <Match key={item} match={item} />
                        )
                    })}
                </MatchContainer>
            </Main>
        </>
    );
};


export default Index;

const Main = styled.main`
    height: 100vh;
    padding-top:70px;
    padding-left: 70px;
    width: 100vw;
    background-color: #0d101e;
`
const InformationsContainer = styled.div`
    margin: 0 10px;

`
const Profilname = styled.p`
    /* text-transform: uppercase; */
    font-size: 30px;
    margin: 10px 0; 
`

const Level = styled.p`
    font-style: italic;
    margin: 0;
`

const ProfilContainer = styled.section`
    height: 200px;
    width: 100%;
    display: flex;
    align-items: center;
    padding: 20px;

`
const ImageContainer = styled.div`
    position: relative;
    width: 120px;
    border: 2px solid #293359;
    overflow: hidden;
    height: 120px;
    border-radius: 20px;
`
const MatchContainer = styled.div`
    width: 100%;
    height: 70%;
    padding: 0 10px;
    &::-webkit-scrollbar {
        display:none
    }
    background-color: #0d101e;
    overflow-y:scroll;
    overflow-x: hidden;
`