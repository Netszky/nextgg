import axios from 'axios';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Champion, { ImageContainer } from '../../components/Champion/Champion';
import Header from '../../components/Header/Header';
import Side from '../../components/Side/Side';

const Index = () => {
    const [champions, setChampions] = useState([]);
    useEffect(() => {
        axios.get("http://ddragon.leagueoflegends.com/cdn/12.22.1/data/fr_FR/champion.json").then((data) => {
            setChampions(data.data.data)
        })
            .catch((err) => {
                console.log(err);
            })
    }, [])

    return (
        <>
            <Header />
            <Side />
            <Main>
                <ListContainer>
                    {Object.keys(champions).map((champ, index) => {
                        return (
                            <Champion key={index} champion={champions[champ]} />
                        )
                    })}
                </ListContainer>
            </Main>
        </>
    );
};

export default Index;

const Main = styled.main`
    background-color: #0d101e;
    height: 100vh;
    width: 100vw;
    padding-top:70px;
    padding-left: 70px;
   
`
const ListContainer = styled.section`
    height: 100%;
    width: 100%;
    padding-top: 80px;
    overflow-y: scroll;
    justify-content: center;
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    overflow-x:hidden;
    
    &::-webkit-scrollbar {
    background: #293359;
    width: 10px;
    }
    &::-webkit-scrollbar-thumb {
        background: #0d101e;
        border-radius: 2px;
    }
    
    &:hover {
        ${ImageContainer} {
            opacity: 0.6
        }
    }
    
`