import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Header from '../../components/Header/Header';
import Side from '../../components/Side/Side';
import axios from 'axios';
import Image from 'next/image';
import {
    motion
} from 'framer-motion';
import Skill from '../../components/Skill/Skill';

const Index = () => {
    const router = useRouter();
    const [champion, setChampion] = useState({});
    const [selectedSpell, setSelectedSpell] = useState()
    const [selectedSpellType, setSelectedSpellType] = useState();
    const formatKey = (key) => {
        if (key?.length === 1) {
            return `000${key}`
        }
        if (key?.length === 2) {
            return `00${key}`
        }
        if (key?.length === 3) {
            return `0${key}`
        }

    }
    useEffect(() => {
        axios.get(`http://ddragon.leagueoflegends.com/cdn/12.22.1/data/fr_FR/champion/${router.query.name}.json`).then((data) => {
            setChampion(data.data.data[router.query.name])
            setSelectedSpell(data.data.data[router.query.name].spells[0])
            setSelectedSpellType(1)
        }).catch((err) => {
            console.log(err);
        })
    }, [router.query])
    const selectSpell = (item, index) => {
        setSelectedSpell(item)
        setSelectedSpellType(index)
    }
    return (
        <>
            <Side />
            <Header />
            <Main>
                <ChampionContainer>
                    <ImageContainer>
                        <Img fill priority={true} sizes='100, 100' alt='champion splash' src={`http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${champion?.id}_0.jpg`} ></Img>
                        <Blur></Blur>
                    </ImageContainer>
                    <TitleContainer>
                        <ChampionTitle initial={{ y: -100 }} animate={{ y: 0 }} transition={{ duration: 0.7, ease: "easeInOut" }}>
                            <p>{champion.name}</p>
                        </ChampionTitle>
                    </TitleContainer>
                </ChampionContainer>
                <LoreContainer>
                    {/* <Separator initial={{ scaleX: 0 }} whileInView={{ scaleX: 1 }} transition={{ duration: 0.7, ease: "easeInOut" }} /> */}
                    <ChampionTitle initial={{ y: -100 }} whileInView={{ y: 0 }} transition={{ duration: 0.7, ease: "easeInOut" }} >
                        <p>Description</p>
                    </ChampionTitle>
                    <p>{champion.lore}</p>
                    <Separator initial={{ scaleX: 0 }} whileInView={{ scaleX: 1 }} transition={{ duration: 0.7, ease: "easeInOut" }} />
                </LoreContainer>
                <SkillsContainer>
                    <ChampionTitle initial={{ y: -100 }} whileInView={{ y: 0 }} transition={{ duration: 0.7, ease: "easeInOut" }} >
                        <p>Compétences</p>
                    </ChampionTitle>
                    <Skills>
                        <Skill selected={selectedSpell} index={0} select={selectSpell} type={"passive"} spell={champion?.passive} />
                        {champion?.spells?.map((item, index) => {
                            return (
                                <Skill selected={selectedSpell} index={index += 1} select={selectSpell} type={"spell"} key={index} spell={item} />
                            )
                        })}
                    </Skills>
                    <SkillDetail>
                        <SkillDescription>
                            <h1>{selectedSpell?.name}</h1>
                            <p>{selectedSpell?.description}</p>
                        </SkillDescription>
                        <VideoContainer>
                            <Video loop controls muted={false} autoPlay={true} src={`https://d28xe8vt774jo5.cloudfront.net/champion-abilities/${formatKey(champion?.key)}/ability_${formatKey(champion?.key)}_${selectedSpellType === 0 ? "P" : selectedSpellType === 1 ? "Q" : selectedSpellType === 2 ? "W" : selectedSpellType === 3 ? "E" : "R"}1.webm`}></Video>
                        </VideoContainer>
                    </SkillDetail>
                    <Separator initial={{ scaleX: 0 }} whileInView={{ scaleX: 1 }} transition={{ duration: 0.7, ease: "easeInOut" }} />
                </SkillsContainer>
            </Main>
        </>
    );
};

export default Index;

const Main = styled.main`
    background-color: #0d101e;
    height: 100vh;
    width: 100vw;
    padding-top:70px;
    /* overflow: hidden; */
    padding-left: 70px;
    overflow-y: scroll;
    scroll-snap-type: y mandatory;
    &::-webkit-scrollbar {
    background: #293359;
    width: 10px;
    }
    &::-webkit-scrollbar-thumb {
        background: #0d101e;
        border-radius: 2px;
    }
   
`
const ChampionContainer = styled.section`
    width: 100%;
    height: 100%;
    min-height: 100vh;
    overflow-y: hidden;
    overflow-x: hidden;
    display: flex;
    flex-direction: column;
    padding: 20px;
    position: relative;
    background-color: #0d101e;
    scroll-snap-align: center;
    align-items: center;
`

const ImageContainer = styled(motion.div)`
    position: relative;
    height: 100%;
    width: 100%;
   
`
const Blur = styled.div`
    height: 100%;
    width: 100%;
    position: absolute;
    backdrop-filter: blur(10px);
    animation: 2000ms cubic-bezier(0.215, 0.61, 0.355, 1) 700ms 1 normal both running blur;
    background-color: #00000025;
    transition: all 0.6s;
    background: linear-gradient(rgba(0,0,0,0), #0d101e 100%);


    @keyframes blur {
        0% {
            backdrop-filter: blur(6px);
        } 100% {
            backdrop-filter: blur(0px);
        }
    }
`

const Img = styled(Image)`
   object-fit: contain;
`
const TitleContainer = styled.div`
    overflow: hidden;
    position: absolute;
    bottom: 20%;
`

const ChampionTitle = styled(motion.div)`
    
    color: #fff;
    font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    text-transform: uppercase;
    font-size: 56px;
    font-weight: bold;
    p{
        margin: 0;
    }
    `

const LoreContainer = styled.section`
    height: 100vh;
    width: 100%;
    background-color: #0d101e;
    display: flex;
    justify-content: center;
    padding: 0 60px;
    flex-direction: column;
    text-align:center;
    align-items: center;
    scroll-snap-align: center;
`
const Separator = styled(motion.div)`
    width: 60%;
    height: 1px;
    background-color: #fff;
    margin-top: 5rem;
`
const SkillsContainer = styled.section`
    height: calc(100vh - 70px);
    width: 100%;
    scroll-snap-align: start;
    background-color: #0d101e;
    display: flex;
    /* padding: 0 60px; */
    flex-direction: column;
    justify-content: center;
    text-align:center;
    align-items: center;
`
const Skills = styled.div`
    display: flex;
    width: 100%;
    height: 20%;
    align-items: center;
    margin-bottom: 20px;
    justify-content: space-evenly;
`
const SkillDetail = styled.div`
 display: flex;
 align-items: center;
 width: 100%;
 padding: 0 2rem;
 height: 60%;
`
const SkillDescription = styled.div`
    padding: 2rem;
    width: 60%;
`
const VideoContainer = styled.div`
    overflow: hidden;
    border-radius: 4px;
    width: 50%;
    height: 100%;
    min-width: 50%;
`
const Video = styled.video`
    width: 100%;
    height: 100%;
    object-fit: cover;
`
