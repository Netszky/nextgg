import '../styles/globals.css'
import Transition from '../transition';

function MyApp({ Component, pageProps, router }) {

  return (

    <Transition>
      <Component {...pageProps} />
    </Transition>
  )
}

export default MyApp
