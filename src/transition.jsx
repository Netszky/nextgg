import { motion, AnimatePresence } from 'framer-motion';
import { useRouter } from 'next/router';

const Transition = ({ children }) => {
    const { asPath } = useRouter();
    return (
        <AnimatePresence
            mode='wait'
        >
            <motion.div
                key={asPath}
                initial={{ x: -200, opacity: 0 }}
                animate={{ x: 0, opacity: 1 }}
                exit={{ x: 200, opacity: 0 }}
                transition={{ duration: 1, ease: "easeOut" }}
            >
                {children}
            </motion.div>
        </AnimatePresence >
    );
};



export default Transition;