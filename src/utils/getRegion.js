const getRegion = (region) => {
    switch (region) {
        case "EUW":
            return "euw1"
        case "EUN":
            return "eun1"
        case "BR":
            return "br1"
        case "JP":
            return "jp1"
        case "KR":
            return "kr"
        case "NA":
            return "na1"
        case "TR":
            return "tr1"
        case "RU":
            return "ru"
        case "OCE":
            return "oc1"
        case "LAN":
            return "la1"
        case "LAS":
            return "la2"
        default:
            return "euw1"
    }
}
export default getRegion;