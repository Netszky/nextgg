/** @type {import('next').NextConfig} */
const nextConfig = {
  // reactStrictMode: true,
  swcMinify: true,
  env: {
    NEXT_PUBLIC_RIOT_API: process.env.NEXT_PUBLIC_RIOT_API,
    NEXT_PUBLIC_DEFAULT_REGION: process.env.NEXT_PUBLIC_DEFAULT_REGION
  },
  images: {
    domains: ['ddragon.leagueoflegends.com']
  },
  compiler: {
    styledComponents: true | {
      displayName: true,
      // Enabled by default.
      ssr: true,
      // Enabled by default.
      fileName: true,
      // Defaults to ["index"].
      meaninglessFileNames: true,
      // Enabled by default.
      cssProp: true,
      // Empty by default.
      minify: true,
    },
  },
}

module.exports = nextConfig
